# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/simon/simon_bitbucket/opengl_tasks2019/task1/592Krivonosov/Main.cpp" "/Users/simon/simon_bitbucket/opengl_tasks2019/cmake-build-debug/task1/592Krivonosov/CMakeFiles/592Krivonosov1.dir/Main.cpp.o"
  "/Users/simon/simon_bitbucket/opengl_tasks2019/task1/592Krivonosov/common/Application.cpp" "/Users/simon/simon_bitbucket/opengl_tasks2019/cmake-build-debug/task1/592Krivonosov/CMakeFiles/592Krivonosov1.dir/common/Application.cpp.o"
  "/Users/simon/simon_bitbucket/opengl_tasks2019/task1/592Krivonosov/common/Camera.cpp" "/Users/simon/simon_bitbucket/opengl_tasks2019/cmake-build-debug/task1/592Krivonosov/CMakeFiles/592Krivonosov1.dir/common/Camera.cpp.o"
  "/Users/simon/simon_bitbucket/opengl_tasks2019/task1/592Krivonosov/common/Mesh.cpp" "/Users/simon/simon_bitbucket/opengl_tasks2019/cmake-build-debug/task1/592Krivonosov/CMakeFiles/592Krivonosov1.dir/common/Mesh.cpp.o"
  "/Users/simon/simon_bitbucket/opengl_tasks2019/task1/592Krivonosov/common/ShaderProgram.cpp" "/Users/simon/simon_bitbucket/opengl_tasks2019/cmake-build-debug/task1/592Krivonosov/CMakeFiles/592Krivonosov1.dir/common/ShaderProgram.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "Clang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GLEW_STATIC"
  "GLFW_DLL"
  "GLM_FORCE_DEPTH_ZERO_TO_ONE"
  "GLM_FORCE_PURE"
  "GLM_FORCE_RADIANS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../external/glew-1.13.0/include"
  "../external/GLM"
  "../external/SOIL/src/SOIL2"
  "../external/Assimp/include"
  "external/Assimp/include"
  "../external/GLFW/include"
  "../external/imgui"
  "../task1/592Krivonosov/common"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/Users/simon/simon_bitbucket/opengl_tasks2019/cmake-build-debug/external/glew-1.13.0/build/cmake/CMakeFiles/glew_s.dir/DependInfo.cmake"
  "/Users/simon/simon_bitbucket/opengl_tasks2019/cmake-build-debug/external/SOIL/CMakeFiles/soil.dir/DependInfo.cmake"
  "/Users/simon/simon_bitbucket/opengl_tasks2019/cmake-build-debug/external/Assimp/code/CMakeFiles/assimp.dir/DependInfo.cmake"
  "/Users/simon/simon_bitbucket/opengl_tasks2019/cmake-build-debug/external/imgui/CMakeFiles/imgui.dir/DependInfo.cmake"
  "/Users/simon/simon_bitbucket/opengl_tasks2019/cmake-build-debug/external/Assimp/contrib/irrXML/CMakeFiles/IrrXML.dir/DependInfo.cmake"
  "/Users/simon/simon_bitbucket/opengl_tasks2019/cmake-build-debug/external/GLFW/src/CMakeFiles/glfw.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
