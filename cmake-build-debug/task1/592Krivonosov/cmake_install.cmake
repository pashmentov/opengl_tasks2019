# Install script for directory: /Users/simon/simon_bitbucket/opengl_tasks2019/task1/592Krivonosov

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Debug")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/task1/592Krivonosov1")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/task1" TYPE EXECUTABLE FILES "/Users/simon/simon_bitbucket/opengl_tasks2019/cmake-build-debug/592Krivonosov1")
  if(EXISTS "$ENV{DESTDIR}/usr/local/task1/592Krivonosov1" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/task1/592Krivonosov1")
    execute_process(COMMAND "/usr/bin/install_name_tool"
      -change "/Users/simon/simon_bitbucket/opengl_tasks2019/cmake-build-debug/libassimpd.4.dylib" "/usr/local/lib/libassimpd.4.dylib"
      -change "/Users/simon/simon_bitbucket/opengl_tasks2019/cmake-build-debug/libglfwd.3.dylib" "lib/libglfwd.3.dylib"
      "$ENV{DESTDIR}/usr/local/task1/592Krivonosov1")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Library/Developer/CommandLineTools/usr/bin/strip" "$ENV{DESTDIR}/usr/local/task1/592Krivonosov1")
    endif()
  endif()
endif()

