#pragma once

#include <vector>

#include "common/Application.hpp"
#include "common/ShaderProgram.hpp"
#include "common/Mesh.hpp"

class TreeMeshGenerator
{
public:
    TreeMeshGenerator(glm::vec3 trunkStart, glm::vec3 trunkEnd, float r);

public:
    MeshPtr getMesh();

private:
    void generateSegmentPoints(glm::vec3 bottomPos, glm::vec3 topPos, float bottomR, int bottomVerticesCount);
    void generateBottomTriangle(size_t bottomIdx1, size_t bottomIdx2, size_t topIdx);
    void generateTopTriangle(size_t bottomIdx, size_t topIdx1, size_t topIdx2);
    void generateNextSegments(glm::vec3 bottomPos, glm::vec3 topPos, float bottomR, int bottomVerticesCount, int depth);
    void generateSegment(glm::vec3 bottomPos, glm::vec3 topPos, float bottomR, int bottomVerticesCount, int depth);

private:
    std::vector<glm::vec3> m_vertices;
    std::vector<glm::vec3> m_normals;

    std::vector<glm::vec3> m_bottomVertices, m_bottomNormals;
    std::vector<glm::vec3> m_topVertices, m_topNormals;
};

class TreeApplication : public Application
{
public:
    void makeScene() override;
    void draw() override;

    auto& getCameraMover() { return _cameraMover; }
    auto& getCamera()      { return _camera;      }
    auto& getWindow()      { return _window;      }
private:
    MeshPtr m_tree;
    ShaderProgramPtr m_shader;
};
