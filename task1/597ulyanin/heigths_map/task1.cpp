#include "heights_map.h"
#include "grid_terrain.h"

#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

#include <iostream>
#include <vector>

/**
Несколько примеров шейдеров
*/
class SampleApplication : public Application
{
public:
    MeshPtr _cube;
    std::vector<ShaderProgramPtr> _shaders;

    MeshPtr TerrainMesh_;
    ShaderProgramPtr TerrainShader_;

    int _currentIndex = 0;

    HeightsMap HeightsMap_;
    GridTerrain GridTerrain_;

    SampleApplication()
        : Application(std::make_unique<TerrainCameraMover>())
        , HeightsMap_("models/hm1.png")
        , GridTerrain_(HeightsMap_)
    {
        std::cerr << HeightsMap_.GetHeight() << " " << HeightsMap_.GetWidth() << std::endl;
    }

    void makeScene() override
    {
        Application::makeScene();

        TerrainShader_ = std::make_shared<ShaderProgram>("shaders_heights_map/shader.vert", "shaders_heights_map/shader.frag");
        TerrainMesh_ = GridTerrain_.GetMesh();


        float scale = 10;
        glm::vec3 rotationAxis(1.f, 0.f, 0.f);
        glm::tmat4x4 rotationMatrix = glm::rotate(glm::pi<float>() / 2 * 0, rotationAxis);
        glm::tmat4x4 translationMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(-scale / 2, -scale / 2, 0.0f));
        glm::vec3 scalingVector = glm::vec3(1.f * scale, 1.f * scale, 0.3);
        glm::tmat4x4 scalingMatrix = glm::scale(scalingVector);
        glm::tmat4x4 modelMatrix = translationMatrix * rotationMatrix * scalingMatrix;

        TerrainMesh_->setModelMatrix(modelMatrix);


        _shaders.push_back(std::make_shared<ShaderProgram>("shaders_heights_map/shader.vert", "shaders_heights_map/shader.frag"));
        _shaders.push_back(std::make_shared<ShaderProgram>("shaders_heights_map/shaderTimeCoord.vert", "shaders_heights_map/shaderTimeCoord.frag"));
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            ImGui::RadioButton("colored", &_currentIndex, 0);
            ImGui::RadioButton("pos animation", &_currentIndex, 1);
        }
        ImGui::End();
    }

    void draw() override
    {
        Application::draw();

        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдер
//        _shaders[_currentIndex]->use();
//
//        Загружаем на видеокарту значения юниформ-переменные: время и матрицы
//        _shaders[_currentIndex]->setFloatUniform("time", (float)glfwGetTime()); //передаем время в шейдер
//
//        _shaders[_currentIndex]->setMat4Uniform("viewMatrix", _camera.viewMatrix);
//        _shaders[_currentIndex]->setMat4Uniform("projectionMatrix", _camera.projMatrix);
//
//        Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
//        _shaders[_currentIndex]->setMat4Uniform("modelMatrix", _cube->modelMatrix());


        // Подключаем шейдер
        assert(TerrainShader_ != nullptr);
        TerrainShader_->use();

        // Загружаем на видеокарту значения юниформ-переменные: время и матрицы
        TerrainShader_->setFloatUniform("time", static_cast<float>(glfwGetTime())); //передаем время в шейдер

        TerrainShader_->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        TerrainShader_->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        // Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
        assert(TerrainMesh_ != nullptr);
        TerrainShader_->setMat4Uniform("modelMatrix", TerrainMesh_->modelMatrix());
        TerrainMesh_->draw();

//        _shaders[_currentIndex]->setMat4Uniform("modelMatrix", _bunny->modelMatrix());
//        _bunny->draw();
    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}